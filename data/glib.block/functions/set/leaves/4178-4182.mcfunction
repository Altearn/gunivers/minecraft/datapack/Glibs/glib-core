execute if score @s glib.blockId matches 4178 run setblock ~ ~ ~ red_stained_glass
execute if score @s glib.blockId matches 4179 run setblock ~ ~ ~ black_stained_glass
execute if score @s glib.blockId matches 4180 run setblock ~ ~ ~ oak_trapdoor[facing=north,half=top,open=true,powered=true,waterlogged=true]
execute if score @s glib.blockId matches 4181 run setblock ~ ~ ~ oak_trapdoor[facing=north,half=top,open=true,powered=true,waterlogged=false]
execute if score @s glib.blockId matches 4182 run setblock ~ ~ ~ oak_trapdoor[facing=north,half=top,open=true,powered=false,waterlogged=true]

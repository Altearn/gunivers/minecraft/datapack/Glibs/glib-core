execute if score @s glib.blockId matches 2023 run setblock ~ ~ ~ oak_stairs[facing=north,half=bottom,shape=inner_left,waterlogged=false]
execute if score @s glib.blockId matches 2024 run setblock ~ ~ ~ oak_stairs[facing=north,half=bottom,shape=inner_right,waterlogged=true]
execute if score @s glib.blockId matches 2025 run setblock ~ ~ ~ oak_stairs[facing=north,half=bottom,shape=inner_right,waterlogged=false]

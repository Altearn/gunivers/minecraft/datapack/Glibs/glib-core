execute if score @s glib.blockId matches 20239 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=low,up=true,waterlogged=false,west=tall]
execute if score @s glib.blockId matches 20240 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=low,up=false,waterlogged=true,west=none]
execute if score @s glib.blockId matches 20241 run setblock ~ ~ ~ deepslate_brick_wall[east=tall,north=none,south=low,up=false,waterlogged=true,west=low]

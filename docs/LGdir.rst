*****
LGdir
*****

This system allow to simply throw a projectile by right-clocking on a ``carrot_on_a_stick``.

To enable this system, you just need to execute the following command in a loop:

.. code-block::

    function gsys.lgdir:main